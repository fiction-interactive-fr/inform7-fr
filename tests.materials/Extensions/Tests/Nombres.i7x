Version 1/210411 of Nombres by Tests begins here.

Volume - La règle Simple unit test

Simple unit test:
	[Les nombres communs à toutes les régions.]
	repeat through the table of nombres en lettres:
		assert "écrire [nombre entry] en lettres" that "[nombre entry in words]" exactly matches the text "[lettres entry]";
	[Les variantes de 70.]
	if variante septante option is active or (Belgian dialect option is active and variante soixante-dix option is not active) or (Swiss dialect option is active and variante soixante-dix option is not active):
		repeat through the table of septante en lettres:
			assert "écrire [nombre entry] en lettres" that "[nombre entry in words]" exactly matches the text "[lettres entry]";
	else:
		repeat through the table of soixante-dix en lettres:
			assert "écrire [nombre entry] en lettres" that "[nombre entry in words]" exactly matches the text "[lettres entry]";
	[Les variantes de 80.]
	if variante huitante option is active or (Swiss dialect option is active and variante quatre-vingts option is not active and variante octante option is not active):
		repeat through the table of huitante en lettres:
			assert "écrire [nombre entry] en lettres" that "[nombre entry in words]" exactly matches the text "[lettres entry]";
	else if variante octante option is active:
		repeat through the table of octante en lettres:
			assert "écrire [nombre entry] en lettres" that "[nombre entry in words]" exactly matches the text "[lettres entry]";
	else:
		repeat through the table of quatre-vingts en lettres:
			assert "écrire [nombre entry] en lettres" that "[nombre entry in words]" exactly matches the text "[lettres entry]";
	[Les variantes de 90.]
	if variante nonante option is active or (Belgian dialect option is active and variante quatre-vingt-dix option is not active) or (Swiss dialect option is active and variante quatre-vingt-dix option is not active):
		repeat through the table of nonante en lettres:
			assert "écrire [nombre entry] en lettres" that "[nombre entry in words]" exactly matches the text "[lettres entry]";
	else:
		repeat through the table of quatre-vingt-dix en lettres:
			assert "écrire [nombre entry] en lettres" that "[nombre entry in words]" exactly matches the text "[lettres entry]";

Volume - Les tableaux des résultats attendus

Book - Les nombres communs à toutes les variantes du français

Table of nombres en lettres
nombre	lettres
0	"zéro"
1	"un"
2	"deux"
3	"trois"
4	"quatre"
5	"cinq"
6	"six"
7	"sept"
8	"huit"
9	"neuf"
10	"dix"
11	"onze"
12	"douze"
13	"treize"
14	"quatorze"
15	"quinze"
16	"seize"
17	"dix-sept"
18	"dix-huit"
19	"dix-neuf"
20	"vingt"
21	"vingt et un"
22	"vingt-deux"
23	"vingt-trois"
24	"vingt-quatre"
25	"vingt-cinq"
26	"vingt-six"
27	"vingt-sept"
28	"vingt-huit"
29	"vingt-neuf"
30	"trente"
31	"trente et un"
32	"trente-deux"
33	"trente-trois"
34	"trente-quatre"
35	"trente-cinq"
36	"trente-six"
37	"trente-sept"
38	"trente-huit"
39	"trente-neuf"
40	"quarante"
41	"quarante et un"
42	"quarante-deux"
43	"quarante-trois"
44	"quarante-quatre"
45	"quarante-cinq"
46	"quarante-six"
47	"quarante-sept"
48	"quarante-huit"
49	"quarante-neuf"
50	"cinquante"
51	"cinquante et un"
52	"cinquante-deux"
53	"cinquante-trois"
54	"cinquante-quatre"
55	"cinquante-cinq"
56	"cinquante-six"
57	"cinquante-sept"
58	"cinquante-huit"
59	"cinquante-neuf"
60	"soixante"
61	"soixante et un"
62	"soixante-deux"
63	"soixante-trois"
64	"soixante-quatre"
65	"soixante-cinq"
66	"soixante-six"
67	"soixante-sept"
68	"soixante-huit"
69	"soixante-neuf"
[On saute les 70, 80 et 90, qui seront testés selon la variante régionale.]
100	"cent" [Pas de « un » avant « cent ».]
123	"cent vingt-trois"
200	"deux cents" [Un « s » à la fin (pareil pour les autres centaines en dessous).]
234	"deux cent trente-quatre" [Pas de « s ».]
300	"trois cents"
400	"quatre cents"
500	"cinq cents"
600	"six cents"
700	"sept cents"
800	"huit cents"
900	"neuf cents"
1000	"mille" [Pas de « un » avant « mille ».]
5000	"cinq mille"
10000	"dix mille"
56000	"cinquante-six mille"
1000000	"un million"
2000000	"deux millions"
300000000	"trois cents millions"	[Un « s » à « cent ».]
1000000000	"un milliard"
-1	"moins un"
-927	"moins neuf cent vingt-sept"

Book - Les 70

Table of soixante-dix en lettres
nombre	lettres
70	"soixante-dix"
71	"soixante et onze"
72	"soixante-douze"
73	"soixante-treize"
74	"soixante-quatorze"
75	"soixante-quinze"
76	"soixante-seize"
77	"soixante-dix-sept"
78	"soixante-dix-huit"
79	"soixante-dix-neuf"

Table of septante en lettres
nombre	lettres
70	"septante"
71	"septante et un"
72	"septante-deux"
73	"septante-trois"
74	"septante-quatre"
75	"septante-cinq"
76	"septante-six"
77	"septante-sept"
78	"septante-huit"
79	"septante-neuf"

Book - Les 80

Table of quatre-vingts en lettres
nombre	lettres
80	"quatre-vingts" [Un « s » à la fin.]
81	"quatre-vingt-un"
82	"quatre-vingt-deux"
83	"quatre-vingt-trois"
84	"quatre-vingt-quatre"
85	"quatre-vingt-cinq"
86	"quatre-vingt-six"
87	"quatre-vingt-sept"
88	"quatre-vingt-huit"
89	"quatre-vingt-neuf"
380	"trois cent quatre-vingts" [Un « s » à « quatre-vingts » mais pas à « cent ».]
80000	"quatre-vingt mille" [Pas de « s » à « quatre-vingt ».]
180000	"cent quatre-vingt mille" [Pas de « s » à « quatre-vingt ».]
987654	"neuf cent quatre-vingt-sept mille six cent cinquante-quatre"
80000000	"quatre-vingts millions" [Un « s » à « quatre-vingts ».]
180200080	"cent quatre-vingts millions deux cent mille quatre-vingts"
1185743829	"un milliard cent quatre-vingt-cinq millions sept cent quarante-trois mille huit cent vingt-neuf"
2147483647	"deux milliards cent quarante-sept millions quatre cent quatre-vingt-trois mille six cent quarante-sept" [Le plus grand nombre Glulx.]
-2147483647	"moins deux milliards cent quarante-sept millions quatre cent quatre-vingt-trois mille six cent quarante-sept" [Le deuxième plus petit nombre Glulx.]
-2147483648	"moins deux milliards cent quarante-sept millions quatre cent quatre-vingt-trois mille six cent quarante-huit" [Le plus petit nombre Glulx.]

Table of huitante en lettres
nombre	lettres
80	"huitante"
81	"huitante et un"
82	"huitante-deux"
83	"huitante-trois"
84	"huitante-quatre"
85	"huitante-cinq"
86	"huitante-six"
87	"huitante-sept"
88	"huitante-huit"
89	"huitante-neuf"
380	"trois cent huitante" [Pas de « s » à « cent ».]
80000	"huitante mille"
180000	"cent huitante mille"
987654	"neuf cent huitante-sept mille six cent cinquante-quatre"
80000000	"huitante millions"
180200080	"cent huitante millions deux cent mille huitante"
1185743829	"un milliard cent huitante-cinq millions sept cent quarante-trois mille huit cent vingt-neuf"
2147483647	"deux milliards cent quarante-sept millions quatre cent huitante-trois mille six cent quarante-sept" [Le plus grand nombre Glulx.]
-2147483647	"moins deux milliards cent quarante-sept millions quatre cent huitante-trois mille six cent quarante-sept" [Le deuxième plus petit nombre Glulx.]
-2147483648	"moins deux milliards cent quarante-sept millions quatre cent huitante-trois mille six cent quarante-huit" [Le plus petit nombre Glulx.]

Table of octante en lettres
nombre	lettres
80	"octante"
81	"octante et un"
82	"octante-deux"
83	"octante-trois"
84	"octante-quatre"
85	"octante-cinq"
86	"octante-six"
87	"octante-sept"
88	"octante-huit"
89	"octante-neuf"
380	"trois cent octante" [Pas de « s » à « cent ».]
80000	"octante mille"
180000	"cent octante mille"
987654	"neuf cent octante-sept mille six cent cinquante-quatre"
80000000	"octante millions"
180200080	"cent octante millions deux cent mille octante"
1185743829	"un milliard cent octante-cinq millions sept cent quarante-trois mille huit cent vingt-neuf"
2147483647	"deux milliards cent quarante-sept millions quatre cent octante-trois mille six cent quarante-sept" [Le plus grand nombre Glulx.]
-2147483647	"moins deux milliards cent quarante-sept millions quatre cent octante-trois mille six cent quarante-sept" [Le deuxième plus petit nombre Glulx.]
-2147483648	"moins deux milliards cent quarante-sept millions quatre cent octante-trois mille six cent quarante-huit" [Le plus petit nombre Glulx.]

Book - Les 90

Table of quatre-vingt-dix en lettres
nombre	lettres
90	"quatre-vingt-dix"
91	"quatre-vingt-onze"
92	"quatre-vingt-douze"
93	"quatre-vingt-treize"
94	"quatre-vingt-quatorze"
95	"quatre-vingt-quinze"
96	"quatre-vingt-seize"
97	"quatre-vingt-dix-sept"
98	"quatre-vingt-dix-huit"
99	"quatre-vingt-dix-neuf"

Table of nonante en lettres
nombre	lettres
90	"nonante"
91	"nonante et un"
92	"nonante-deux"
93	"nonante-trois"
94	"nonante-quatre"
95	"nonante-cinq"
96	"nonante-six"
97	"nonante-sept"
98	"nonante-huit"
99	"nonante-neuf"

Nombres ends here.
