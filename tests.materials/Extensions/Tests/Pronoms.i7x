Version 1/201008 of Pronoms by Tests begins here.

Book - Pour le joueur

Part - Tu

Simple unit test:
	repeat through the table of tu:
		now le story viewpoint is point de vue entry;
		assert "pronom «[_]tu[_]», [story viewpoint]" that "[tu]" exactly matches the text "[résultat minuscule entry]";
		assert "pronom «[_]Tu[_]», [story viewpoint]" that "[Tu]" exactly matches the text "[résultat majuscule entry]";
	[On teste séparément le cas où le joueur est une femme.]
	now Céleste est dans la location;
	now le player est Céleste;
	now le story viewpoint est third person singular;
	assert "pronom «[_]tu[_]», [story viewpoint] féminin" that "[tu]" exactly matches the text "elle";
	assert "pronom «[_]Tu[_]», [story viewpoint] féminin" that "[Tu]" exactly matches the text "Elle";
	now le story viewpoint est third person plural;
	assert "pronom «[_]tu[_]», [story viewpoint] féminin" that "[tu]" exactly matches the text "elles";
	assert "pronom «[_]Tu[_]», [story viewpoint] féminin" that "[Tu]" exactly matches the text "Elles";
	reset the world.

Table of tu
point de vue	résultat minuscule	résultat majuscule
first person singular	"je"	"Je"
second person singular	"tu"	"Tu"
third person singular	"il"	"Il"
first person plural	"nous"	"Nous"
second person plural	"vous"	"Vous"
third person plural	"ils"	"Ils"

Part - Tu-j'

Simple unit test:
	repeat through the table of tu-j':
		now le story viewpoint is point de vue entry;
		assert "pronom «[_]tu-j['][_]», [story viewpoint]" that "[tu-j']" exactly matches the text "[résultat minuscule entry]";
		assert "pronom «[_]Tu-j['][_]», [story viewpoint]" that "[Tu-j']" exactly matches the text "[résultat majuscule entry]";
	[On teste séparément le cas où le joueur est une femme.]
	now Céleste est dans la location;
	now le player est Céleste;
	now le story viewpoint est third person singular;
	assert "pronom «[_]tu-j['][_]», [story viewpoint] féminin" that "[tu-j']" exactly matches the text "elle ";
	assert "pronom «[_]Tu-j['][_]», [story viewpoint] féminin" that "[Tu-j']" exactly matches the text "Elle ";
	now le story viewpoint est third person plural;
	assert "pronom «[_]tu-j['][_]», [story viewpoint] féminin" that "[tu-j']" exactly matches the text "elles ";
	assert "pronom «[_]Tu-j['][_]», [story viewpoint] féminin" that "[Tu-j']" exactly matches the text "Elles ";
	reset the world.

Table of tu-j'
point de vue	résultat minuscule	résultat majuscule
first person singular	"j[']"	"J[']"
second person singular	"tu "	"Tu "
third person singular	"il "	"Il "
first person plural	"nous "	"Nous "
second person plural	"vous "	"Vous "
third person plural	"ils "	"Ils "

Part - Si tu

Simple unit test:
	repeat through the table of si tu:
		now le story viewpoint is point de vue entry;
		assert "pronom «[_]si tu[_]», [story viewpoint]" that "[si tu]" exactly matches the text "[résultat minuscule entry]";
		assert "pronom «[_]Si tu[_]», [story viewpoint]" that "[Si tu]" exactly matches the text "[résultat majuscule entry]";
	[On teste séparément le cas où le joueur est une femme.]
	now Céleste est dans la location;
	now le player est Céleste;
	now le story viewpoint est third person singular;
	assert "pronom «[_]si tu[_]», [story viewpoint] féminin" that "[si tu]" exactly matches the text "si elle";
	assert "pronom «[_]Si tu[_]», [story viewpoint] féminin" that "[Si tu]" exactly matches the text "Si elle";
	now le story viewpoint est third person plural;
	assert "pronom «[_]si tu[_]», [story viewpoint] féminin" that "[si tu]" exactly matches the text "si elles";
	assert "pronom «[_]Si tu[_]», [story viewpoint] féminin" that "[Si tu]" exactly matches the text "Si elles";
	reset the world.

Table of si tu
point de vue	résultat minuscule	résultat majuscule
first person singular	"si je"	"Si je"
second person singular	"si tu"	"Si tu"
third person singular	"s'il"	"S'il"
first person plural	"si nous"	"Si nous"
second person plural	"si vous"	"Si vous"
third person plural	"s'ils"	"S'ils"

Part - Te

Simple unit test:
	repeat through the table of te:
		now le story viewpoint is point de vue entry;
		assert "pronom «[_]te[_]», [story viewpoint]" that "[te]" exactly matches the text "[résultat minuscule entry]";
		assert "pronom «[_]Te[_]», [story viewpoint]" that "[Te]" exactly matches the text "[résultat majuscule entry]";
	[On teste séparément le cas où le joueur est une femme.]
	now Céleste est dans la location;
	now le player est Céleste;
	now le story viewpoint est third person singular;
	assert "pronom «[_]te[_]», [story viewpoint] féminin" that "[te]" exactly matches the text "la";
	assert "pronom «[_]Te[_]», [story viewpoint] féminin" that "[Te]" exactly matches the text "La";
	reset the world.

Table of te
point de vue	résultat minuscule	résultat majuscule
first person singular	"me"	"Me"
second person singular	"te"	"Te"
third person singular	"le"	"Le"
first person plural	"nous"	"Nous"
second person plural	"vous"	"Vous"
third person plural	"les"	"Les"

Part - T'

Simple unit test:
	repeat through the table of t':
		now le story viewpoint is point de vue entry;
		assert "pronom «[_]t['][_]», [story viewpoint]" that "[t']" exactly matches the text "[résultat minuscule entry]";
		assert "pronom «[_]T['][_]», [story viewpoint]" that "[T']" exactly matches the text "[résultat majuscule entry]";
	reset the world.

Table of t'
point de vue	résultat minuscule	résultat majuscule
first person singular	"m[']"	"M[']"
second person singular	"t[']"	"T[']"
third person singular	"l[']"	"L[']"
first person plural	"nous "	"Nous "
second person plural	"vous "	"Vous "
third person plural	"les "	"Les "

Part - Te-lui

Simple unit test:
	repeat through the table of te-lui:
		now le story viewpoint is point de vue entry;
		assert "pronom «[_]te-lui[_]», [story viewpoint]" that "[te-lui]" exactly matches the text "[résultat minuscule entry]";
		assert "pronom «[_]Te-lui[_]», [story viewpoint]" that "[Te-lui]" exactly matches the text "[résultat majuscule entry]";
	reset the world.

Table of te-lui
point de vue	résultat minuscule	résultat majuscule
first person singular	"me"	"Me"
second person singular	"te"	"Te"
third person singular	"lui"	"Lui"
first person plural	"nous"	"Nous"
second person plural	"vous"	"Vous"
third person plural	"leur"	"Leur"

Part - T'-lui

Simple unit test:
	repeat through the table of t'-lui:
		now le story viewpoint is point de vue entry;
		assert "pronom «[_]t[']-lui[_]», [story viewpoint]" that "[t'-lui]" exactly matches the text "[résultat minuscule entry]";
		assert "pronom «[_]T[']-lui[_]», [story viewpoint]" that "[T'-lui]" exactly matches the text "[résultat majuscule entry]";
	reset the world.

Table of t'-lui
point de vue	résultat minuscule	résultat majuscule
first person singular	"m[']"	"M[']"
second person singular	"t[']"	"T[']"
third person singular	"lui "	"Lui "
first person plural	"nous "	"Nous "
second person plural	"vous "	"Vous "
third person plural	"leur "	"Leur "

Part - Te-se

Simple unit test:
	repeat through the table of te-se:
		now le story viewpoint is point de vue entry;
		assert "pronom «[_]te-se[_]», [story viewpoint]" that "[te-se]" exactly matches the text "[résultat minuscule entry]";
		assert "pronom «[_]Te-se[_]», [story viewpoint]" that "[Te-se]" exactly matches the text "[résultat majuscule entry]";
	reset the world.

Table of te-se
point de vue	résultat minuscule	résultat majuscule
first person singular	"me"	"Me"
second person singular	"te"	"Te"
third person singular	"se"	"Se"
first person plural	"nous"	"Nous"
second person plural	"vous"	"Vous"
third person plural	"se"	"Se"

Part - T'-s'

Simple unit test:
	repeat through the table of t'-s':
		now le story viewpoint is point de vue entry;
		assert "pronom «[_]t[']-s['][_]», [story viewpoint]" that "[t'-s']" exactly matches the text "[résultat minuscule entry]";
		assert "pronom «[_]T[']-s['][_]», [story viewpoint]" that "[T'-s']" exactly matches the text "[résultat majuscule entry]";
	reset the world.

Table of t'-s'
point de vue	résultat minuscule	résultat majuscule
first person singular	"m[']"	"M[']"
second person singular	"t[']"	"T[']"
third person singular	"s[']"	"S[']"
first person plural	"nous "	"Nous "
second person plural	"vous "	"Vous "
third person plural	"s[']"	"S[']"

Part - Toi

Simple unit test:
	repeat through the table of toi:
		now le story viewpoint is point de vue entry;
		assert "pronom «[_]toi[_]», [story viewpoint]" that "[toi]" exactly matches the text "[résultat minuscule entry]";
		assert "pronom «[_]Toi[_]», [story viewpoint]" that "[Toi]" exactly matches the text "[résultat majuscule entry]";
	[On teste séparément le cas où le joueur est une femme.]
	now Céleste est dans la location;
	now le player est Céleste;
	now le story viewpoint est third person singular;
	assert "pronom «[_]toi[_]», [story viewpoint] féminin" that "[toi]" exactly matches the text "elle";
	assert "pronom «[_]Toi[_]», [story viewpoint] féminin" that "[Toi]" exactly matches the text "Elle";
	now le story viewpoint est third person plural;
	assert "pronom «[_]toi[_]», [story viewpoint] féminin" that "[toi]" exactly matches the text "elles";
	assert "pronom «[_]Toi[_]», [story viewpoint] féminin" that "[Toi]" exactly matches the text "Elles";
	reset the world.

Table of toi
point de vue	résultat minuscule	résultat majuscule
first person singular	"moi"	"Moi"
second person singular	"toi"	"Toi"
third person singular	"lui"	"Lui"
first person plural	"nous"	"Nous"
second person plural	"vous"	"Vous"
third person plural	"eux"	"Eux"

Part - De toi

Simple unit test:
	repeat through the table of de toi:
		now le story viewpoint is point de vue entry;
		assert "pronom «[_]de toi[_]», [story viewpoint]" that "[de toi]" exactly matches the text "[résultat minuscule entry]";
		assert "pronom «[_]De toi[_]», [story viewpoint]" that "[De toi]" exactly matches the text "[résultat majuscule entry]";
	[On teste séparément le cas où le joueur est une femme.]
	now Céleste est dans la location;
	now le player est Céleste;
	now le story viewpoint est third person singular;
	assert "pronom «[_]de toi[_]», [story viewpoint] féminin" that "[de toi]" exactly matches the text "d'elle";
	assert "pronom «[_]De toi[_]», [story viewpoint] féminin" that "[De toi]" exactly matches the text "D'elle";
	now le story viewpoint est third person plural;
	assert "pronom «[_]de toi[_]», [story viewpoint] féminin" that "[de toi]" exactly matches the text "d'elles";
	assert "pronom «[_]De toi[_]», [story viewpoint] féminin" that "[De toi]" exactly matches the text "D'elles";
	reset the world.

Table of de toi
point de vue	résultat minuscule	résultat majuscule
first person singular	"de moi"	"De moi"
second person singular	"de toi"	"De toi"
third person singular	"de lui"	"De lui"
first person plural	"de nous"	"De nous"
second person plural	"de vous"	"De vous"
third person plural	"d'eux"	"D'eux"

Part - Le tien

Simple unit test:
	repeat through the table of le tien:
		now le story viewpoint is point de vue entry;
		now the prior named object is objet concerné entry;
		assert "pronom «[_]le tien[_]», [story viewpoint] avec [prior named object]" that "[le tien]" exactly matches the text "[résultat minuscule entry]";
		assert "pronom «[_]Le tien[_]», [story viewpoint] avec [prior named object]" that "[Le tien]" exactly matches the text "[résultat majuscule entry]";
	reset the world.

Table of le tien
point de vue	objet concerné	résultat minuscule	résultat majuscule
first person singular	concept-MS	"le mien"	"Le mien"
first person singular	concepts-MP	"les miens"	"Les miens"
first person singular	notion-FS	"la mienne"	"La mienne"
first person singular	notions-FP	"les miennes"	"Les miennes"
second person singular	concept-MS	"le tien"	"Le tien"
second person singular	concepts-MP	"les tiens"	"Les tiens"
second person singular	notion-FS	"la tienne"	"La tienne"
second person singular	notions-FP	"les tiennes"	"Les tiennes"
third person singular	concept-MS	"le sien"	"Le sien"
third person singular	concepts-MP	"les siens"	"Les siens"
third person singular	notion-FS	"la sienne"	"La sienne"
third person singular	notions-FP	"les siennes"	"Les siennes"
first person plural	concept-MS	"le nôtre"	"Le nôtre"
first person plural	concepts-MP	"les nôtres"	"Les nôtres"
first person plural	notion-FS	"la nôtre"	"La nôtre"
first person plural	notions-FP	"les nôtres"	"Les nôtres"
second person plural	concept-MS	"le vôtre"	"Le vôtre"
second person plural	concepts-MP	"les vôtres"	"Les vôtres"
second person plural	notion-FS	"la vôtre"	"La vôtre"
second person plural	notions-FP	"les vôtres"	"Les vôtres"
third person plural	concept-MS	"le leur"	"Le leur"
third person plural	concepts-MP	"les leurs"	"Les leurs"
third person plural	notion-FS	"la leur"	"La leur"
third person plural	notions-FP	"les leurs"	"Les leurs"

Part - Ton

[Pas un pronom, mais on le met là car il dépend du point de vue de l'histoire.]

Simple unit test:
	repeat through the table of ton:
		now le story viewpoint is point de vue entry;
		assert "pronom «[_]ton[_]», [story viewpoint]" that "[ton]" exactly matches the text "[résultat minuscule entry]";
		assert "pronom «[_]Ton[_]», [story viewpoint]" that "[Ton]" exactly matches the text "[résultat majuscule entry]";
	reset the world.

Table of ton
point de vue	résultat minuscule	résultat majuscule
first person singular	"mon"	"Mon"
second person singular	"ton"	"Ton"
third person singular	"son"	"Son"
first person plural	"notre"	"Notre"
second person plural	"votre"	"Votre"
third person plural	"leur"	"Leur"

Part - Ta

[Pas un pronom, mais on le met là car il dépend du point de vue de l'histoire.]

Simple unit test:
	repeat through the table of ta:
		now le story viewpoint is point de vue entry;
		assert "pronom «[_]ta[_]», [story viewpoint]" that "[ta]" exactly matches the text "[résultat minuscule entry]";
		assert "pronom «[_]Ta[_]», [story viewpoint]" that "[Ta]" exactly matches the text "[résultat majuscule entry]";
	reset the world.

Table of ta
point de vue	résultat minuscule	résultat majuscule
first person singular	"ma"	"Ma"
second person singular	"ta"	"Ta"
third person singular	"sa"	"Sa"
first person plural	"notre"	"Notre"
second person plural	"votre"	"Votre"
third person plural	"leur"	"Leur"

Part - Tes

[Pas un pronom, mais on le met là car il dépend du point de vue de l'histoire.]

Simple unit test:
	repeat through the table of tes:
		now le story viewpoint is point de vue entry;
		assert "pronom «[_]tes[_]», [story viewpoint]" that "[tes]" exactly matches the text "[résultat minuscule entry]";
		assert "pronom «[_]Tes[_]», [story viewpoint]" that "[Tes]" exactly matches the text "[résultat majuscule entry]";
	reset the world.

Table of tes
point de vue	résultat minuscule	résultat majuscule
first person singular	"mes"	"Mes"
second person singular	"tes"	"Tes"
third person singular	"ses"	"Ses"
first person plural	"nos"	"Nos"
second person plural	"vos"	"Vos"
third person plural	"leurs"	"Leurs"

Part - Ton, ta et tes (avec un objet)

[Pas des pronoms, mais on les met là car ils dépendent du point de vue de l'histoire.]

Simple unit test:
	repeat through the table of ton-ta-tes:
		now le story viewpoint is point de vue entry;
		assert "déterminant «[_]ton/ta/tes[_]», [story viewpoint] avec [objet concerné entry]" that "[ton objet concerné entry]" exactly matches the text "[résultat minuscule entry]";
		assert "déterminant «[_]Ton/Ta/Tes[_]», [story viewpoint] avec [objet concerné entry]" that "[Ton objet concerné entry]" exactly matches the text "[résultat majuscule entry]";
	reset the world.

Table of ton-ta-tes
point de vue	objet concerné	résultat minuscule	résultat majuscule
first person singular	concept-MS	"mon concept-MS"	"Mon concept-MS"
first person singular	aconcept-MS	"mon aconcept-MS"	"Mon aconcept-MS"
first person singular	concepts-MP	"mes concepts-MP"	"Mes concepts-MP"
first person singular	aconcepts-MP	"mes aconcepts-MP"	"Mes aconcepts-MP"
first person singular	notion-FS	"ma notion-FS"	"Ma notion-FS"
first person singular	anotion-FS	"mon anotion-FS"	"Mon anotion-FS"
first person singular	notions-FP	"mes notions-FP"	"Mes notions-FP"
first person singular	anotions-FP	"mes anotions-FP"	"Mes anotions-FP"
second person singular	concept-MS	"ton concept-MS"	"Ton concept-MS"
second person singular	aconcept-MS	"ton aconcept-MS"	"Ton aconcept-MS"
second person singular	concepts-MP	"tes concepts-MP"	"Tes concepts-MP"
second person singular	aconcepts-MP	"tes aconcepts-MP"	"Tes aconcepts-MP"
second person singular	notion-FS	"ta notion-FS"	"Ta notion-FS"
second person singular	anotion-FS	"ton anotion-FS"	"Ton anotion-FS"
second person singular	notions-FP	"tes notions-FP"	"Tes notions-FP"
second person singular	anotions-FP	"tes anotions-FP"	"Tes anotions-FP"
third person singular	concept-MS	"son concept-MS"	"Son concept-MS"
third person singular	aconcept-MS	"son aconcept-MS"	"Son aconcept-MS"
third person singular	concepts-MP	"ses concepts-MP"	"Ses concepts-MP"
third person singular	aconcepts-MP	"ses aconcepts-MP"	"Ses aconcepts-MP"
third person singular	notion-FS	"sa notion-FS"	"Sa notion-FS"
third person singular	anotion-FS	"son anotion-FS"	"Son anotion-FS"
third person singular	notions-FP	"ses notions-FP"	"Ses notions-FP"
third person singular	anotions-FP	"ses anotions-FP"	"Ses anotions-FP"
first person plural	concept-MS	"notre concept-MS"	"Notre concept-MS"
first person plural	aconcept-MS	"notre aconcept-MS"	"Notre aconcept-MS"
first person plural	concepts-MP	"nos concepts-MP"	"Nos concepts-MP"
first person plural	aconcepts-MP	"nos aconcepts-MP"	"Nos aconcepts-MP"
first person plural	notion-FS	"notre notion-FS"	"Notre notion-FS"
first person plural	anotion-FS	"notre anotion-FS"	"Notre anotion-FS"
first person plural	notions-FP	"nos notions-FP"	"Nos notions-FP"
first person plural	anotions-FP	"nos anotions-FP"	"Nos anotions-FP"
second person plural	concept-MS	"votre concept-MS"	"Votre concept-MS"
second person plural	aconcept-MS	"votre aconcept-MS"	"Votre aconcept-MS"
second person plural	concepts-MP	"vos concepts-MP"	"Vos concepts-MP"
second person plural	aconcepts-MP	"vos aconcepts-MP"	"Vos aconcepts-MP"
second person plural	notion-FS	"votre notion-FS"	"Votre notion-FS"
second person plural	anotion-FS	"votre anotion-FS"	"Votre anotion-FS"
second person plural	notions-FP	"vos notions-FP"	"Vos notions-FP"
second person plural	anotions-FP	"vos anotions-FP"	"Vos anotions-FP"
third person plural	concept-MS	"leur concept-MS"	"Leur concept-MS"
third person plural	aconcept-MS	"leur aconcept-MS"	"Leur aconcept-MS"
third person plural	concepts-MP	"leurs concepts-MP"	"Leurs concepts-MP"
third person plural	aconcepts-MP	"leurs aconcepts-MP"	"Leurs aconcepts-MP"
third person plural	notion-FS	"leur notion-FS"	"Leur notion-FS"
third person plural	anotion-FS	"leur anotion-FS"	"Leur anotion-FS"
third person plural	notions-FP	"leurs notions-FP"	"Leurs notions-FP"
third person plural	anotions-FP	"leurs anotions-FP"	"Leurs anotions-FP"

Book - Pour les autres objets

[TODO: Je n'ai pas effectué d'assertions pour vérifier le cas où le prior named object est le joueur. Il faudrait idéalement le faire, mais ça devrait rarement arriver en jeu.]

Part - Celui

Simple unit test:
	repeat through the table of celui:
		now le prior named object est l' objet concerné entry;
		assert "pronom «[_]celui[_]» avec [objet concerné entry]" that "[celui]" exactly matches the text "[résultat minuscule entry]";
		assert "pronom «[_]Celui[_]» avec [objet concerné entry]" that "[Celui]" exactly matches the text "[résultat majuscule entry]";

Table of celui
objet concerné	résultat minuscule	résultat majuscule
concept-MS	"celui"	"Celui"
notion-FS	"celle"	"Celle"
concepts-MP	"ceux"	"Ceux"
notions-FP	"celles"	"Celles"

Part - Il

Simple unit test:
	repeat through the table of il:
		now le prior named object est l' objet concerné entry;
		assert "pronom «[_]il[_]»  avec [objet concerné entry]" that "[il]" exactly matches the text "[résultat minuscule entry]";
		assert "pronom «[_]Il[_]» avec [objet concerné entry]" that "[Il]" exactly matches the text "[résultat majuscule entry]";

Table of il
objet concerné	résultat minuscule	résultat majuscule
concept-MS	"il"	"Il"
notion-FS	"elle"	"Elle"
concepts-MP	"ils"	"Ils"
notions-FP	"elles"	"Elles"

Part - Le

Simple unit test:
	repeat through the table of le:
		now le prior named object est l' objet concerné entry;
		assert "pronom «[_]le[_]» avec [objet concerné entry]" that "[le]" exactly matches the text "[résultat minuscule entry]";
		assert "pronom «[_]Le[_]» avec [objet concerné entry]" that "[Le]" exactly matches the text "[résultat majuscule entry]";

Table of le
objet concerné	résultat minuscule	résultat majuscule
concept-MS	"le"	"Le"
notion-FS	"la"	"La"
concepts-MP	"les"	"Les"
notions-FP	"les"	"Les"

Part - L'

Simple unit test:
	repeat through the table of l':
		now le prior named object est l' objet concerné entry;
		assert "pronom «[_]l['][_]» avec [objet concerné entry]" that "[l']" exactly matches the text "[résultat minuscule entry]";
		assert "pronom «[_]L['][_]» avec [objet concerné entry]" that "[L']" exactly matches the text "[résultat majuscule entry]";

Table of l'
objet concerné	résultat minuscule	résultat majuscule
concept-MS	"l[']"	"L[']"
notion-FS	"l[']"	"L[']"
concepts-MP	"les "	"Les "
notions-FP	"les "	"Les "

Part - Lui

Simple unit test:
	repeat through the table of lui:
		now le prior named object est l' objet concerné entry;
		assert "pronom «[_]lui[_]» avec [objet concerné entry]" that "[lui]" exactly matches the text "[résultat minuscule entry]";
		assert "pronom «[_]Lui[_]» avec [objet concerné entry]" that "[Lui]" exactly matches the text "[résultat majuscule entry]";

Table of lui
objet concerné	résultat minuscule	résultat majuscule
concept-MS	"lui"	"Lui"
notion-FS	"lui"	"Lui"
concepts-MP	"leur"	"Leur"
notions-FP	"leur"	"Leur"

Part - Son

[Pas des pronoms, mais on les met là quand même parce qu'il va avec le reste.]

Simple unit test:
	repeat through the table of son:
		now le prior named object est l' objet concerné entry;
		assert "déterminant «[_]son[_]» pour [objet concerné entry]" that "[son]" exactly matches the text "[résultat minuscule entry]";
		assert "déterminant «[_]Son[_]» pour [objet concerné entry]" that "[Son]" exactly matches the text "[résultat majuscule entry]";

Table of son
objet concerné	résultat minuscule	résultat majuscule
concept-MS	"son"	"Son"
concepts-MP	"leur"	"Leur"

Part - Sa

[Pas des pronoms, mais on les met là quand même parce qu'il va avec le reste.]

Simple unit test:
	repeat through the table of sa:
		now le prior named object est l' objet concerné entry;
		assert "déterminant «[_]sa[_]» pour [objet concerné entry]" that "[sa]" exactly matches the text "[résultat minuscule entry]";
		assert "déterminant «[_]Sa[_]» pour [objet concerné entry]" that "[Sa]" exactly matches the text "[résultat majuscule entry]";

Table of sa
objet concerné	résultat minuscule	résultat majuscule
concept-MS	"sa"	"Sa"
concepts-MP	"leur"	"Leur"

Part - Ses

[Pas des pronoms, mais on les met là quand même parce qu'il va avec le reste.]

Simple unit test:
	repeat through the table of ses:
		now le prior named object est l' objet concerné entry;
		assert "déterminant «[_]ses[_]» pour [objet concerné entry]" that "[ses]" exactly matches the text "[résultat minuscule entry]";
		assert "déterminant «[_]Ses[_]» pour [objet concerné entry]" that "[Ses]" exactly matches the text "[résultat majuscule entry]";

Table of ses
objet concerné	résultat minuscule	résultat majuscule
concept-MS	"ses"	"Ses"
concepts-MP	"leurs"	"Leurs"

Part - Son, sa, ses (avec un objet)

[Pas des pronoms, mais on les met là quand même parce qu'il va avec le reste.]

Simple unit test:
	repeat through the table of son-sa-ses:
		assert "déterminant «[_]son/sa/ses/leur/leurs[_]» avec [objet concerné entry], propriétaire singulier" that "[regarding concept-MS][son objet concerné entry]" exactly matches the text "[résultat minuscule entry]";
		assert "déterminant «[_]Son/Sa/Ses/Leur/Leurs[_]» avec [objet concerné entry], propriétaire singulier" that "[regarding concept-MS][Son objet concerné entry]" exactly matches the text "[résultat majuscule entry]";
	repeat through the table of leur-leurs:
		assert "déterminant «[_]son/sa/ses/leur/leurs[_]» avec [objet concerné entry] propriétaire pluriel" that "[regarding concepts-MP][son objet concerné entry]" exactly matches the text "[résultat minuscule entry]";
		assert "déterminant «[_]Son/Sa/Ses/Leur/Leurs[_]» avec [objet concerné entry], propriétaire pluriel" that "[regarding concepts-MP][Son objet concerné entry]" exactly matches the text "[résultat majuscule entry]";

Table of son-sa-ses
objet concerné	résultat minuscule	résultat majuscule
concept-MS	"son concept-MS"	"Son concept-MS"
aconcept-MS	"son aconcept-MS"	"Son aconcept-MS"
concepts-MP	"ses concepts-MP"	"Ses concepts-MP"
aconcepts-MP	"ses aconcepts-MP"	"Ses aconcepts-MP"
notion-FS	"sa notion-FS"	"Sa notion-FS"
anotion-FS	"son anotion-FS"	"Son anotion-FS"
notions-FP	"ses notions-FP"	"Ses notions-FP"
anotions-FP	"ses anotions-FP"	"Ses anotions-FP"

Table of leur-leurs
objet concerné	résultat minuscule	résultat majuscule
concept-MS	"leur concept-MS"	"Leur concept-MS"
aconcept-MS	"leur aconcept-MS"	"Leur aconcept-MS"
concepts-MP	"leurs concepts-MP"	"Leurs concepts-MP"
aconcepts-MP	"leurs aconcepts-MP"	"Leurs aconcepts-MP"
notion-FS	"leur notion-FS"	"Leur notion-FS"
anotion-FS	"leur anotion-FS"	"Leur anotion-FS"
notions-FP	"leurs notions-FP"	"Leurs notions-FP"
anotions-FP	"leurs anotions-FP"	"Leurs anotions-FP"

Part - Le sien

Simple unit test:
	repeat through the table of le sien:
		now le prior named object est l' objet concerné entry;
		assert "pronom «[_]le sien[_]» avec [objet concerné entry]" that "[le sien]" exactly matches the text "[résultat minuscule entry]";
		assert "pronom «[_]Le sien[_]» avec [objet concerné entry]" that "[Le sien]" exactly matches the text "[résultat majuscule entry]";

Table of le sien
objet concerné	résultat minuscule	résultat majuscule
concept-MS	"le sien"	"Le sien"
notion-FS	"la sienne"	"La sienne"
concepts-MP	"les siens"	"Les siens"
notions-FP	"les siennes"	"Les siennes"

Part - Le leur

Simple unit test:
	repeat through the table of le leur:
		now le prior named object est l' objet concerné entry;
		assert "pronom «[_]le leur[_]» avec [objet concerné entry]" that "[le leur]" exactly matches the text "[résultat minuscule entry]";
		assert "pronom «[_]Le leur[_]» avec [objet concerné entry]" that "[Le leur]" exactly matches the text "[résultat majuscule entry]";

Table of le leur
objet concerné	résultat minuscule	résultat majuscule
concept-MS	"le leur"	"Le leur"
notion-FS	"la leur"	"La leur"
concepts-MP	"les leurs"	"Les leurs"
notions-FP	"les leurs"	"Les leurs"

Pronoms ends here.
