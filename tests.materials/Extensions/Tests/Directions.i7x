Version 1/201011 of Directions by Tests begins here.

Book - Déterminants

Simple unit test:
	repeat through the table of déterminants directions:
		let D be direction entry;
		assert "article défini pour [D]" that "[le D]" exactly matches the text "[défini minuscule entry]";
		assert "article défini majuscule pour [D]" that "[Le D]" exactly matches the text "[défini majuscule entry]";
		assert "article indéfini pour [D]" that "[un D]" exactly matches the text "[indéfini minuscule entry]";
		assert "article indéfini majuscule pour [D]" that "[Un D]" exactly matches the text "[indéfini majuscule entry]";

Table of déterminants directions
direction	défini minuscule	défini majuscule	indéfini minuscule	indéfini majuscule
nord	"le nord"	"Le nord"	"le nord"	"Le nord"
sud	"le sud"	"Le sud"	"le sud"	"Le sud"
est	"l'est"	"L'est"	"l'est"	"L'est"
ouest	"l'ouest"	"L'ouest"	"l'ouest"	"L'ouest"
nord-est	"le nord-est"	"Le nord-est"	"le nord-est"	"Le nord-est"
sud-ouest	"le sud-ouest"	"Le sud-ouest"	"le sud-ouest"	"Le sud-ouest"
sud-est	"le sud-est"	"Le sud-est"	"le sud-est"	"Le sud-est"
nord-ouest	"le nord-ouest"	"Le nord-ouest"	"le nord-ouest"	"Le nord-ouest"
dedans	"dedans"	"Dedans"	"dedans"	"Dedans"
dehors	"dehors"	"Dehors"	"dehors"	"Dehors"
haut	"le haut"	"Le haut"	"le haut"	"Le haut"
bas	"le bas"	"Le bas"	"le bas"	"Le bas"

Directions ends here.
