# Extension française pour Inform 7

Ce dépôt contient les diverses ressources permettant d'écrire des fictions interactives avec Inform 7.

**IMPORTANT : LA TRADUCTION NE FONCTIONNE QU'AVEC LA VERSION 6L38 D'INFORM 7. À CAUSE D'UN BUG D'INFORM 7, ELLE NE FONCTIONNE PAS AVEC LA DERNIÈRE VERSION, 6M62 ! (Sur macOS, il faut télécharger la version d'Inform intitulée « Interim 2019 release » et sélectionner la version d'Inform dans l'onglet « Settings » du projet.)**

## Utilisation

Si vous n'avez pas d'expérience avec Inform 7, [le tutoriel](http://www.fiction-interactive.fr/ecrire-une-fiction-interactive-avec-inform-7-partie-1-premiers-pas/) sur [Fiction-interactive.fr](http://www.fiction-interactive.fr/) est un bon point de départ.

Si vous êtes déjà à l'aise avec Inform 7, la première partie du tutoriel explique tout de même comment installer l'extension française ; vous pouvez également lire les instructions ci-dessous.

### Installation

- Télécharger les fichiers `French Language.i7x`, `about.txt` et optionnellement `Experimental French Features.i7x` contenus dans ce dépôt.
- Remplacer le fichier `about.txt` de l'installation d'Inform par celui du dépôt. Son emplacement selon le système d'exploitation :
	- Windows : `C:\Program Files (x86)\Inform 7\Internal\Languages\French\about.txt`.
	- macOS : clic droit sur l'icône d'Inform puis « Afficher le contenu du paquet » ; de là, le fichier se trouve dans `Contents/Resources/retrospective/6L38/Languages/French/about.txt`.
	- Linux : `/usr/share/gnome-inform7/Extensions/Reserved/Languages/French/about.txt`
ou `/usr/share/gnome-inform7/Languages/French/about.txt`. (Je ne suis pas sûr pour Linux.)
- Installer comme n'importe quelle autre extension Inform 7 les extensions téléchargées (menu « File » puis « Install Extension »).
- Pour écrire un jeu en français, ajouter `(in French)` après le titre du jeu. Il ne faut pas écrire la ligne `Include French Language by Nathanael Marion` ! Si vous souhaitez utiliser les fonctionnalités expérimentales, vous pouvez inclure `Experimental French Features` normalement. Voir l'exemple ci-dessous.

```
"Ma super fiction interactive" by Natrium729 (in French)

[La ligne suivante est optionnelle.]
Include Experimental French Features by Nathanael Marion.

La cuisine est un endroit.
```

### Fonctionnalités expérimentales

Toutes les fonctionnalités expérimentales sont dans l'extension `Experimental French Features.i7x`. Elle permet d'écrire une grande partie de son code source en français (syntaxe et adjectifs). La plupart de ces fonctionnalités devraient fonctionner sans problème, même s'il y a toujours un petit risque que quelque chose ne fonctionne pas comme prévu.

### Documentation

Tout ce qui est spécifique au français est expliqué dans la documentation des extensions, disponible dans Inform 7 après les avoir installées (onglet « Extensions », puis cliquer sur « French Language » ou « Experimental French Features »).
